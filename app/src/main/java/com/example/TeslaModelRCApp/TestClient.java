package com.example.TeslaModelRCApp;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.TextView;

import com.google.android.gms.common.server.converter.StringToIntConverter;

import org.ws4d.coap.core.CoapClient;
import org.ws4d.coap.core.CoapConstants;
import org.ws4d.coap.core.connection.BasicCoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapChannelManager;
import org.ws4d.coap.core.connection.api.CoapClientChannel;
import org.ws4d.coap.core.enumerations.CoapMediaType;
import org.ws4d.coap.core.enumerations.CoapRequestCode;
import org.ws4d.coap.core.messages.api.CoapRequest;
import org.ws4d.coap.core.messages.api.CoapResponse;
import org.ws4d.coap.core.rest.CoapData;
import org.ws4d.coap.core.tools.Encoder;
import org.ws4d.coap.example.basics.Server;

import java.net.InetAddress;
import java.net.UnknownHostException;

public class TestClient implements CoapClient {

    private static final boolean exitAfterResponse = false;
    private CoapChannelManager channelManager;
    private CoapClientChannel clientChannel;
    private CoapRequest request;

    String payload = "";

    Handler refresh = new Handler(Looper.getMainLooper());

    public void start() {
        String sAddress = "192.168.4.1";
        int sPort = CoapConstants.COAP_DEFAULT_PORT;
        this.channelManager = BasicCoapChannelManager.getInstance();
        this.clientChannel = null;
        try {
            this.clientChannel = this.channelManager.connect(this,
                    InetAddress.getByName(sAddress), sPort);
        } catch (UnknownHostException e) {
            e.printStackTrace();
            Log.v("*******", "===clientChannel FAILED TO CONNECT===");
        }

    }

    public void connectChannel(String serverAddress, int serverPort) {
        this.channelManager = BasicCoapChannelManager.getInstance();
        this.clientChannel = null;
        try {
            this.clientChannel = this.channelManager.connect(this, InetAddress.getByName(serverAddress), serverPort);
            if (this.clientChannel == null) {
                Log.v("*******", "===clientChannel was NULL===");
            }
        } catch (Exception e) {
            System.err.println(e.getLocalizedMessage());
            System.exit(-1);
        }
    }

    public void sendPut(String path, String data){
        if(clientChannel == null)
        {
            Log.v("*******", "===clientChannel was NULL===");
            connectChannel("192.168.4.1", CoapConstants.COAP_DEFAULT_PORT);
        }
        else{
            request = clientChannel.createRequest(CoapRequestCode.PUT, path, true);
            request.setPayload(new CoapData(data, CoapMediaType.text_plain));
            clientChannel.sendMessage(request);
            Log.v("*******", "Sending message request: PUT");
        }
    }

    public void sendRequest(String path) {
        if (null == this.clientChannel) {
            Log.v("*******", "===clientChannel was NULL===");
            return;
        }
        request = this.clientChannel.createRequest(CoapRequestCode.GET,
                path, true);
//            request = this.clientChannel.createRequest(CoapRequestCode.GET, "/.well-known/core", true);
        this.clientChannel.sendMessage(request);
    }

    @Override
    public void onResponse(CoapClientChannel channel, CoapResponse
            response) {
        if (response.getPayload() != null) {
            payload = Encoder.ByteToString(response.getPayload());
            Log.v("*******", "Response: " + response.toString() + " payload: " + payload);


//            if (payload.equals("tmrc:heartbeat")) {
//                try {
//                    sensorfragment.getINSTANCE().heartbeat.setText("Heartbeat Received!");
//                    Log.v("*********", "Heartbeat TEXTVIEW UPDATED!!!");
//                } catch (Exception e) {
//                    Log.v("*********", "Heartbeat TEXTVIEW WAS NULL!!!");
//                }
//            }

            if(payload.length() >= 11) {

                refresh.post(new Runnable() {
                    public void run() {
                            if (payload.equals("tmrc:heartbeat")) {
                                    sensorfragment.getINSTANCE().heartbeat.setText("Heartbeat Received!");
                                    Log.v("*********", "Heartbeat TEXTVIEW UPDATED!!!");
                            }

                            if (!payload.equals("tmrc:heartbeat") ){
                                Log.v("*********", (payload));
                                try {
                                    sensorfragment.getINSTANCE().middletxt = payload.substring(0, 2);
                                    sensorfragment.getINSTANCE().lefttxt = payload.substring(3, 5);
                                    sensorfragment.getINSTANCE().righttxt = payload.substring(6, 8);
                                    sensorfragment.getINSTANCE().reartxt = payload.substring(9, 11);
                                    sensorfragment.getINSTANCE().middle = Integer.parseInt(sensorfragment.getINSTANCE().middletxt);
                                    sensorfragment.getINSTANCE().left = Integer.parseInt(sensorfragment.getINSTANCE().lefttxt);
                                    sensorfragment.getINSTANCE().right = Integer.parseInt(sensorfragment.getINSTANCE().righttxt);
                                    sensorfragment.getINSTANCE().rear = Integer.parseInt(sensorfragment.getINSTANCE().reartxt);
                                } catch (Exception e) {

                                }
                                sensorfragment.getINSTANCE().middleProgressBar.setMax(99);
                                sensorfragment.getINSTANCE().leftProgressBar.setMax(99);
                                sensorfragment.getINSTANCE().rightProgressBar.setMax(99);
                                sensorfragment.getINSTANCE().rearProgressBar.setMax(99);

                                sensorfragment.getINSTANCE().middleProgressBar.setProgress(99);
                                sensorfragment.getINSTANCE().leftProgressBar.setProgress(99);
                                sensorfragment.getINSTANCE().rightProgressBar.setProgress(99);
                                sensorfragment.getINSTANCE().rearProgressBar.setProgress(99);

                                sensorfragment.getINSTANCE().middleProgressBar.setProgress(99 - sensorfragment.getINSTANCE().middle);
                                sensorfragment.getINSTANCE().leftProgressBar.setProgress(99 - sensorfragment.getINSTANCE().left);
                                sensorfragment.getINSTANCE().rightProgressBar.setProgress(99 - sensorfragment.getINSTANCE().right);
                                sensorfragment.getINSTANCE().rearProgressBar.setProgress(99 - sensorfragment.getINSTANCE().rear);

                                sensorfragment.getINSTANCE().middletxt = "";
                                sensorfragment.getINSTANCE().lefttxt = "";
                                sensorfragment.getINSTANCE().righttxt = "";
                                sensorfragment.getINSTANCE().reartxt = "";

                                sensorfragment.getINSTANCE().middleDebug.setText(sensorfragment.getINSTANCE().middletxt);
                                sensorfragment.getINSTANCE().leftDebug.setText(sensorfragment.getINSTANCE().lefttxt);
                                sensorfragment.getINSTANCE().rightDebug.setText(sensorfragment.getINSTANCE().righttxt);
                                sensorfragment.getINSTANCE().rearDebug.setText(sensorfragment.getINSTANCE().reartxt);

                                sensorfragment.getINSTANCE().getTextView().get(0).setText(sensorfragment.getINSTANCE().lefttxt);
                            }

                    }
                });

            }


        } else {
            System.out.println("Response: " + response.toString());
        }
        if (this.exitAfterResponse) {
            Log.v("*******", "===END===");
        }
    }

    @Override
    public void onMCResponse(CoapClientChannel channel, CoapResponse
            response, InetAddress srcAddress, int srcPort) {
        Log.v("*******","Response from "+ srcAddress.toString() + ":" + srcPort + " - " + response.toString());
    }

    @Override
    public void onConnectionFailed(CoapClientChannel channel,
                                   boolean notReachable, boolean resetByServer) {
        Log.v("*******", "Connection Failed from phone APP");
        this.start();
    }


}
