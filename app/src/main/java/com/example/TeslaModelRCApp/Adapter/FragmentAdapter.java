package com.example.TeslaModelRCApp.Adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.example.TeslaModelRCApp.mapfragment;
import com.example.TeslaModelRCApp.sensorfragment;

public class FragmentAdapter extends FragmentPagerAdapter {
    Context context;

    public FragmentAdapter(FragmentManager fm, Context context){
        super(fm);
        this.context = context;
    }

    @Override
    public Fragment getItem(int position){
        if(position == 0)
            return mapfragment.getINSTANCE();
        else if (position == 1)
            return sensorfragment.getINSTANCE();
        else
            return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        switch(position){
            case 0:
                return "Map";
            case 1:
                return "Sensor Info";
        }
        return "";
    }
}
